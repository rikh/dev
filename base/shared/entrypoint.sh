#!/bin/sh
set -e

echo 'Starting development environment'
echo 'Attach to the container by using `docker-compose exec base fish` from `~/git/dev/base`'
echo '(Or boot the service at system startup and make an alias for attaching to the container)'

# Starting some shell keeps container alive.
bash
