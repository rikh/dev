" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

Plug 'vim-scripts/vim-auto-save'

Plug 'junegunn/fzf'

Plug 'neoclide/coc.nvim', {'branch': 'release'}

call plug#end()
