# dev_u_can_act

To avoid name collisions of containers put `COMPOSE_PROJECT_NAME=dev_u_can_act` in a `.env` file.

To get a shell inside a running service run
```
docker-compose exec <service name> sh
```
(This is useful when, for example, one would like to inspect the database of a running web service.)

To get a shell inside a container which is not running
```
docker compose run <service name> sh
```
