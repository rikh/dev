# dev

The idea of this repository is to have reproducible development environments for various projects.
This is achieved by using Docker(-compose).
Symbolic links called `link` are relative and assume the repository folder to be a sibling of this `dev` repository folder. 

A base image is extended with project specific elements.
The production environment could be more closely simulated by using the production Dockerfile and extending that with project specific elements.
However, this is hard to do since all production settings (for example, ports) need to be extended and overriden.
Also, the dev environment have to be defined in a platform agnostic way.
Therefore, we use a base development image and extend it for certain projects if needed.

## Running a development environment
Execute 
```
docker-compose build
docker-compose run <service>
```
where `<service>` is the service name as listed in `docker-compose.yml`.

## Relative symbolic link
source: https://superuser.com/questions/146231

1. Go to the directory you want the link to reside in
2. Run the command `ln -s ../some/other/file linkname`
